FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /source

# install deps and cache to speed up
COPY source.sln .
COPY src/program.csproj src/program.csproj
COPY test/test.csproj test/test.csproj
RUN dotnet restore

# test, publish app and libraries
COPY src src
COPY test test
ARG TESTSPEC="None"
ARG paymenturl=""
RUN if [ "$TESTSPEC" != "None" ]; then \
    dotnet test --filter TestCategory=$TESTSPEC && \
    dotnet publish -c debug -o /app --no-restore; \
  else \
    dotnet publish -c debug -o /app --no-restore; \
  fi

# final stage/image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["dotnet", "program.dll"]
