using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using program.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace program.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionItemsController : ControllerBase
    {
        private readonly ActionItemContext _context;
        private readonly string _paymentUrl;

        public ActionItemsController(ActionItemContext context)
        {
            _context = context;
            _paymentUrl = Environment.GetEnvironmentVariable("paymenturl");
            if (string.IsNullOrEmpty(_paymentUrl)
                || !IsValidUrl(_paymentUrl)) 
            {
                throw new ArgumentException("paymentUrl is not valid");
            }
        }

        private bool IsValidUrl(string url)
        {
            return Uri.TryCreate(url, UriKind.Absolute, out var uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        // GET: api/ActionItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActionItem>>> GetActionItem()
        {
            return await _context.ActionItem.ToListAsync();
        }

        // GET: api/ActionItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActionItem>> GetActionItem(long id)
        {
            var actionItem = await _context.ActionItem.FindAsync(id);

            if (actionItem == null)
            {
                return NotFound();
            }

            return actionItem;
        }

        // PUT: api/ActionItems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActionItem(long id, ActionItem actionItem)
        {
            if (id != actionItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(actionItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActionItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<Tuple<ActionItem, string>> PostActionItem([FromForm] ActionItem actionItem)
        {
            // Make sure request is not null
            if (actionItem == null)
            {
                return Tuple.Create(new ActionItem(), "Request is invalid");
            }

            //Only transfer is allowed
            if (actionItem.Action != "transfer")
            {
                return Tuple.Create(new ActionItem(), "You can only transfer an amount");
            }

            var amount = actionItem.Amount;
            if (amount == int.MaxValue || amount == int.MinValue)
            {
                throw new OverflowException();
            }

            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            // Short-circuit to prevent DoS
            if (amount == 0)
            {
                return Tuple.Create(actionItem, "Successfully transfered $0");
            }

            // Only use known parameters and validated values
            var formContent = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("amount", amount.ToString()),
                new KeyValuePair<string, string>("action", actionItem.Action)
            };

            var formUrlEncodedContent = new FormUrlEncodedContent(formContent);

            var httpClient = new HttpClient();
            
            HttpResponseMessage result = await httpClient.PostAsync(_paymentUrl, formUrlEncodedContent);
            var response = await result.Content.ReadAsStringAsync();
            //Print response from Payment service
            Console.WriteLine(response);

            _context.ActionItem.Add(actionItem);
            await _context.SaveChangesAsync();

            // Don't return every response from payment to users
            if (response != $"Successfully transfered ${amount}")
            {
                return Tuple.Create(actionItem, "Internal server error");
            }

            return Tuple.Create(actionItem, response);
            
        }


        // DELETE: api/ActionItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActionItem>> DeleteActionItem(long id)
        {
            var actionItem = await _context.ActionItem.FindAsync(id);
            if (actionItem == null)
            {
                return NotFound();
            }

            _context.ActionItem.Remove(actionItem);
            await _context.SaveChangesAsync();

            return actionItem;
        }

        private bool ActionItemExists(long id)
        {
            return _context.ActionItem.Any(e => e.Id == id);
        }
    }
}
