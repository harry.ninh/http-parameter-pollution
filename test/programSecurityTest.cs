using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using NUnit.Framework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using program.Controllers;
using program.Models;

namespace program.Tests
{
    [TestFixture, Category("security")]
    public class programSecurityTest
    {
        DefaultHttpContext httpContext;
        [SetUp]
        public void Setup()
        {
            //If you manually debugging, run Payment service and set the url below
            //Environment.SetEnvironmentVariable("paymenturl", "http://172.17.0.1:5001/api/payment");
            httpContext = new DefaultHttpContext();
            httpContext.Request.Scheme = "http";
            httpContext.Request.Host = new HostString("localhost");
        }

        [Test]
        public void should_disallow_negative_amount()
        {
            //Arrange
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", "transfer" },
                { "amount", "-100" }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };


            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "transfer",
                Amount = -100
            };

            //Act & Assert
            
            //Assert.That(await controller.PostActionItem(expectedActionItem), Throws.Exception);
            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await controller.PostActionItem(expectedActionItem));

        }
        [Test]
        public void should_disallow_IntMax_amount()
        {
            //Arrange
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", "transfer" },
                { "amount", Int32.MaxValue.ToString() }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };


            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "transfer",
                Amount = Int32.MaxValue
            };

            //Act & Assert
            Assert.ThrowsAsync<OverflowException>(async () => await controller.PostActionItem(expectedActionItem));

        }       
        [Test]
        public void should_disallow_IntMin_amount()
        {
            //Arrange
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", "transfer" },
                { "amount", Int32.MinValue.ToString() }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };


            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "transfer",
                Amount = Int32.MinValue
            };

            //Act & Assert
            Assert.ThrowsAsync<OverflowException>(async () => await controller.PostActionItem(expectedActionItem));

        }
        [Test]
        public async Task should_handle_mutiple_actions()
        {
            //Arrange
            string[] actions = new string[]{"transfer", "withdraw"}; 
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", actions },
                { "amount", "100" }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };


            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "transfer",
                Amount = 100
            };
            //Act
            var result = await controller.PostActionItem(expectedActionItem);

            //Assert
            Assert.That(result, Is.InstanceOf<Tuple<ActionItem, string>>());
            Assert.That(result.Item1, Is.Not.Null);
            Assert.That(result.Item2, Is.Not.Null);
            Assert.AreEqual(expectedActionItem.Action, result.Item1.Action);
            Assert.AreEqual("Successfully transfered $100", result.Item2);

        }
        [Test]
        public async Task should_handle_mutiple_amounts()
        {
            //Arrange
            string[] amounts = new string[]{"100", "200"}; 
            var formCol = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>
            {
                { "action", "transfer" },
                { "amount", amounts }
            });
            httpContext.Request.ContentType = "application/x-www-form-urlencoded";
            httpContext.Request.Form = formCol;
            var context = new ActionItemContext(Utilities.TestDbContextOptions());
            var controller = new ActionItemsController(context);
            controller.ControllerContext = new ControllerContext {
                HttpContext = httpContext
            };

            var expectedActionItem = new ActionItem
            {
                Id = 1,
                Action = "transfer",
                Amount = 100
            };
            //Act
            var result = await controller.PostActionItem(expectedActionItem);

            //Assert
            Assert.That(result, Is.InstanceOf<Tuple<ActionItem, string>>());
            Assert.That(result.Item1, Is.Not.Null);
            Assert.That(result.Item2, Is.Not.Null);
            Assert.AreEqual(expectedActionItem.Action, result.Item1.Action);
            Assert.AreEqual("Successfully transfered $100", result.Item2);
        }
    }

}
